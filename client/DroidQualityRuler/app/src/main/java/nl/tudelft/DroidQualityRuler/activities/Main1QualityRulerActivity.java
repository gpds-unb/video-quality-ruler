package nl.tudelft.DroidQualityRuler.activities;

import nl.tudelft.DroidQualityRuler.controllers.UserActionController;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class Main1QualityRulerActivity extends AbstractQualityRulerActivity {
    @Override
    void initUserActionController() {
        this.userActionController = new UserActionController();
        this.userActionController.setMaxVideoCounter(24);
    }

    @Override
    String getTypeOfActivity() {
        return "Main1";
    }

    @Override
    protected Class getNextActivity() {
        return BreakInstructionActivity.class;
    }
}
