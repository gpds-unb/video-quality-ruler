package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/7/14.at TU Delft
 */
public class FirstPresentationInstructionActivity extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<h1>Presentation</h1>"
                + "<p>Hello, <strong>%s</strong></p>"
                + "<p>Welcome! Thank you for participating in this experiment. It is divided in 3 stages:</p>"
                + "<ol>"
                + "<li>Free Viewing,</li>"
                + "<li>Guided Practicing,</li>"
                + "<li>Main Session.</li>"
                + "</ol>"
                + "<p>At the beginning of each stage, It will be presented what you have to do. Please, "
                + "read the instructions <em>carefully</em>.</p>"
                + "<h1>Free Viewing Session</h1>"
                + "<p>Now you will watch a sequence of 7 videos. "
                + "These videos contain several types of defects/artifacts. </p>"
                + "<p>Please, watch the videos as if you were watching TV.</p>"
                + "<p>Click OK to start playing the videos.</p>"
                + "<p>Hello Yi!</p>"
                + "</body>"
                + "</html>";
        return String.format(txt, name);
    }

    @Override
    protected Class getNextActivity() {
        return FreeViewingRequestVideoActivity.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return false;
    }
}