package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/8/14.at TU Delft
 */
public class FreeViewingInstructionsActivity extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<h1>Free Viewing Session</h1>"
                + "<p><strong>Look at the screen now.</strong> You should be watching a video now. </p>"
                + "Press the button OK after the videos playing (gray screen)."
                + "</body>"
                + "</html>";
        return String.format(txt, name);
    }


    @Override
    protected Class getNextActivity() {
        return TrainingInstructionActivity.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return  118 * 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return true;
    }
}
