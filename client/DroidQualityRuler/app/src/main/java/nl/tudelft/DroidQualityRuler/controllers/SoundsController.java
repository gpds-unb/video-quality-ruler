package nl.tudelft.DroidQualityRuler.controllers;

import android.content.Context;
import android.media.MediaPlayer;
import nl.tudelft.DroidQualityRuler.R;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/17/14.at TU Delft
 */
public class SoundsController {
    private static Boolean isPlaying = false;

    public static void playEnabledRuler(final Context context) {
        play(context, R.raw.enabling);
    }

    public static void playButtonClick(final Context context) {
        play(context, R.raw.buttonclick);
    }

    public static void playBIP(final Context context) {
        play(context, R.raw.bip);
    }

    private static void play(final Context context, final Integer element) {
        new Thread() {
            public void run() {
                try {
                    if (!isPlaying)
                        playSound();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            private void playSound() throws InterruptedException {
                isPlaying = true;
                MediaPlayer player = MediaPlayer.create(context, element);
                player.start();
                Thread.sleep(player.getDuration());
                player.release();
                isPlaying = false;
            }
        }.start();
    }
}
