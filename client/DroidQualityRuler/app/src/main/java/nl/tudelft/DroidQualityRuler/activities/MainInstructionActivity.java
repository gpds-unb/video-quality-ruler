package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class MainInstructionActivity extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<h1>Main Session</h1>"
                + "<p>Did you understand how to use the Quality Ruler?</p>"
                + "<p>From now your answers will be registered.</p>"
                + "<p>If you have any question, please contact me.</p>"
                + "<p>Please, click on the button to start.</p>"
                + "</body>"
                + "</html>";
        return String.format(txt, name);
    }


    @Override
    protected Class getNextActivity() {
        return Main1QualityRulerActivity.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return false;
    }
}
