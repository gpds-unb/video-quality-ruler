package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/18/14.at TU Delft
 */
public class TrainingInstructionActivity3 extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<h1>Guided Practicing</h1>"
                + "<p>Now, we show you how the Quality Ruler works.</p>"
                + "<p>Watch the video on the screen and then press OK button to continue.</p>"
                + "</body>"
                + "</html>";
        return txt;
    }

    @Override
    protected Class getNextActivity() {
        return TrainingInstructionActivity4.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 46 * 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return true;
    }
}
