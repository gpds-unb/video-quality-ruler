package nl.tudelft.DroidQualityRuler.data;

import org.json.JSONException;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class OpenFileRequestPacket extends SocketPacket {
    private String name;
    private Integer age;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected void createJSONString() throws JSONException {
        super.createJSONString();
        js.put("age", getAge().toString());
        js.put("name", getName().toString());

    }
}
