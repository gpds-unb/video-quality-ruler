package nl.tudelft.DroidQualityRuler.activities;

import nl.tudelft.DroidQualityRuler.controllers.UserActionController;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class TrainingQualityRulerActivity extends AbstractQualityRulerActivity {
    @Override
    void initUserActionController() {
        this.userActionController = new UserActionController();
        this.userActionController.setMaxVideoCounter(7);
    }

    @Override
    String getTypeOfActivity() {
        return "Training";
    }

    @Override
    protected Class getNextActivity() {
        return MainInstructionActivity.class;
    }
}
