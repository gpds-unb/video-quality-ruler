package nl.tudelft.DroidQualityRuler.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import nl.tudelft.DroidQualityRuler.R;
import nl.tudelft.DroidQualityRuler.controllers.SocketConnectionController;
import nl.tudelft.DroidQualityRuler.data.SocketPacket;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class FinalActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finalscreen);
        setButtonAction();
        requestCloseFile();
    }

    private void setButtonAction() {
        Button btnNextScreen = (Button) findViewById(R.id.button_end);
        btnNextScreen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        requestCloseFile();
    }

    private void requestCloseFile() {
        new Thread() {
            public void run() {
                closeFile();
                closeWindow();
                closeSession();
            }

            private void sendPacketInfo(String info) {
                SocketPacket socketPacket = new SocketPacket();
                socketPacket.setStepCode(info);
                SocketConnectionController.send(socketPacket.toJSONString());
            }

            private void closeSession() {
                sendPacketInfo("CloseSession");
            }

            private void closeWindow() {
                sendPacketInfo("CloseWindow");
            }

            private void closeFile() {
                sendPacketInfo("CloseFile");
            }
        }.start();
    }
}
