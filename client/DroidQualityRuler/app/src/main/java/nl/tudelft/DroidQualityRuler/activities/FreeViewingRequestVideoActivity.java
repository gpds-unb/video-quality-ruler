package nl.tudelft.DroidQualityRuler.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import nl.tudelft.DroidQualityRuler.controllers.SocketConnectionController;
import nl.tudelft.DroidQualityRuler.data.ClientDataPacket;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/8/14.at TU Delft
 */
public class FreeViewingRequestVideoActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread() {
            public void run() {
                ClientDataPacket fvr = new ClientDataPacket();
                fvr.setStepCode("FreeViewing");
                SocketConnectionController.send(fvr.toJSONString());
            }
        }.start();
        Intent nextScreen = new Intent(getApplicationContext(), FreeViewingInstructionsActivity.class);
        startActivity(nextScreen);
    }
}
