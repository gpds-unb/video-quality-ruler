package nl.tudelft.DroidQualityRuler.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import nl.tudelft.DroidQualityRuler.R;
import nl.tudelft.DroidQualityRuler.controllers.SoundsController;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/7/14.at TU Delft
 */
public abstract class AbstractInstructionsActivity extends Activity {
    protected String name;
    protected String age;
    Button btnNextScreen;
    Timer buttonTimer = new Timer();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instructionsscreen);
        setVariables();
        createWebView();
        setButtonAction();
    }

    abstract protected String getText();

    abstract protected Class getNextActivity();

    abstract protected Integer getWaitingTimeForEnableButton();

    abstract protected Boolean isBepedInstruction();

    private void createWebView() {
        WebView webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(false);
        webView.loadData(getText(), "text/html", "UTF-8");
    }

    private void setButtonAction() {
        btnNextScreen = (Button) findViewById(R.id.btnNextScreen2);
        btnNextScreen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent nextScreen = new Intent(getApplicationContext(), getNextActivity());
                nextScreen.putExtra("name", name);
                nextScreen.putExtra("age", age);
                startActivity(nextScreen);
            }
        });
        btnNextScreen.setEnabled(false);
        setButtonEnabledTimer();
    }

    private void setButtonEnabledTimer() {
        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnNextScreen.setEnabled(true);
                        playBeepIfAvailable();
                    }
                });
            }
        }, getWaitingTimeForEnableButton());
    }

    private void playBeepIfAvailable() {
        if (isBepedInstruction())
            SoundsController.playBIP(this);
    }

    private void setVariables() {
        Intent i = getIntent();
        name = i.getStringExtra("name");
        age = i.getStringExtra("age");
    }
}