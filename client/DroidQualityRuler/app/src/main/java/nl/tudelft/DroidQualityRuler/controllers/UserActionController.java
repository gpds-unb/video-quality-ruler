package nl.tudelft.DroidQualityRuler.controllers;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 6/20/14.at TU Delft
 */
public class UserActionController {
    private Integer windowCounter = 0;
    private Integer maxVideoCounter = 0;

    public UserActionController() {
    }

    public Integer getMaxVideoCounter() {
        return this.maxVideoCounter;
    }

    public void setMaxVideoCounter(Integer maxVideoCounter) {
        this.maxVideoCounter = maxVideoCounter;
    }

    public void sumWindowCounter(Integer v) {
        this.windowCounter += v;
    }

    public void subWindowCounter(Integer v) {
        this.windowCounter -= v;
    }

    public void increaseWindowCounter() {
        sumWindowCounter(1);
    }

    public void decreaseWindowCounter() {
        subWindowCounter(1);
    }

    public void setWindowCounter(Integer windowCounter) {
        this.windowCounter = windowCounter;
    }

    public Integer getWindowCounter() {
        return this.windowCounter;
    }

}
