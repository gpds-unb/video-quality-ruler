package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/18/14.at TU Delft
 */
public class TrainingInstructionActivity5 extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<p>Did you understand how to use the ruler now?</p>"
                + "<p>In the last step, you should noticed that the interface is disabled for a while. "
                + "This is because the interface is enabled only after you watch the video once. "
                + "Therefore, fix your attention only at the video. </p>"
                + "<p>After you watched the video once, the interface on the tablet is enabled and the video "
                + "is repeated until you match the annoyance of the image and the video.</p>"
                + "<p>Lets make a second attempt to familiarize you with the interface.</p>"
                + "<p>Click the OK button to continue.</p>"
                + "</body>"
                + "</html>";
        return txt;
    }

    @Override
    protected Class getNextActivity() {
        return TrainingQualityRulerActivity.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return true;
    }
}
