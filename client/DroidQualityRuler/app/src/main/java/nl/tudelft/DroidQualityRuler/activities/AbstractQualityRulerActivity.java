package nl.tudelft.DroidQualityRuler.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.*;
import android.widget.Gallery.LayoutParams;
import nl.tudelft.DroidQualityRuler.R;
import nl.tudelft.DroidQualityRuler.controllers.ImagesController;
import nl.tudelft.DroidQualityRuler.controllers.SocketConnectionController;
import nl.tudelft.DroidQualityRuler.controllers.SoundsController;
import nl.tudelft.DroidQualityRuler.controllers.UserActionController;
import nl.tudelft.DroidQualityRuler.data.ClientDataPacket;
import nl.tudelft.DroidQualityRuler.data.SocketPacket;
import nl.tudelft.DroidQualityRuler.ruler.ImageAdapter;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 6/19/14.
 */
public abstract class AbstractQualityRulerActivity extends Activity implements
        AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory {

    protected UserActionController userActionController;
    private Random random = new Random();
    private ClientDataPacket userOpinion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTreadPolicyCompatibility();
        initUserActionController();
        ImagesController.loadImages();
        requestSessionStart();
        refreshActivityContent();
    }

    abstract void initUserActionController();

    abstract String getTypeOfActivity();

    abstract protected Class getNextActivity();

    private void checkIfNeedToChangeActivity() {
        if (userActionController.getWindowCounter() >= userActionController.getMaxVideoCounter()) {
            Intent nextScreen = new Intent(getApplicationContext(), getNextActivity());
            startActivity(nextScreen);
        }
    }

    private void refreshActivityContent() {
        Integer windowCounter = userActionController.getWindowCounter();
        Integer maxVideoCounter = userActionController.getMaxVideoCounter();
        if (windowCounter < maxVideoCounter) {
            String activityType = getTypeOfActivity().replaceAll("[^A-Za-z]","");
            String title = String.format("%s Session: %d/%d", activityType, windowCounter + 1, maxVideoCounter);
            getActionBar().setTitle(title);
            setContentView(R.layout.qualityrulerscreen);
            startImageSwitcher();
            createUserOpinion();
            createGalery();
            createButtonEvents();
            setButtonChooseEnabledTimer();
        }
    }

    private void createUserOpinion() {
        userOpinion = new ClientDataPacket();
        userOpinion.setVideo(userActionController.getWindowCounter());
        userOpinion.setStepCode(getTypeOfActivity());
    }

    private void createGalery() {
        Gallery gallery = (Gallery) findViewById(R.id.gallery);
        gallery.setAdapter(new ImageAdapter(this.getBaseContext()));
        gallery.setOnItemSelectedListener(this);
        Integer startPosition = random.nextInt(ImagesController.getImages().length);
        gallery.setSelection(startPosition);
        gallery.setAlpha(0);
    }

    private void createButtonEvents() {
        createButtonEvent(R.id.button_choose);
    }


    private void createButtonEvent(final Integer buttonCode) {
        Button button = (Button) findViewById(buttonCode);
        button.setEnabled(false);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                addButtonAction(buttonCode);
            }
        });
    }

    private void addButtonAction(Integer buttonCode) {
        switch (buttonCode) {
            case R.id.button_choose:
                createButtonChooseEvent();
                break;
        }
    }

    private void createTreadPolicyCompatibility() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void requestSessionStart() {
        new Thread() {
            public void run() {
                startVideo();
            }

            private void startVideo() {
                SocketPacket socketPacket = new SocketPacket();
                socketPacket.setStepCode("StartRequest" + getTypeOfActivity());
                SocketConnectionController.send(socketPacket.toJSONString());
            }
        }.start();
    }

    private void createButtonChooseEvent() {
        String data = userOpinion.toJSONString();
        SocketConnectionController.send(data);
        userActionController.increaseWindowCounter();
        checkIfNeedToChangeActivity();
        playButtonSoundIfAvailable();
        refreshActivityContent();
    }

    private void setButtonChooseEnabledTimer() {
        final Gallery gallery = (Gallery) findViewById(R.id.gallery);
        final Button button = (Button) findViewById(R.id.button_choose);
        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        button.setEnabled(true);
                        gallery.setAlpha(100);
                        playRulerSoundIfAvailable();
                    }
                });
            }
        }, 16 * 1000);
    }

    private void playButtonSoundIfAvailable() {
        SoundsController.playButtonClick(this);
    }

    private void playRulerSoundIfAvailable() {
        SoundsController.playEnabledRuler(this);
    }

    private void startImageSwitcher() {
        // imageSwitcher = (ImageSwitcher) findViewById(R.id.switcher);
//        imageSwitcher.setFactory(this);
//        imageSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
//        imageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        //imageSwitcher.setImageResource(ImagesController.getImageAt(position));
        userOpinion.setImage(position);
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFFFFFF);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return i;
    }
}
