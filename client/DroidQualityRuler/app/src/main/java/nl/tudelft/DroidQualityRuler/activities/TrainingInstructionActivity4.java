package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/18/14.at TU Delft
 */
public class TrainingInstructionActivity4 extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<p>Did you understand how to use the ruler?</p>"
                + "<p>The mouse pointer in the last video indicates how should slide the images at the ruler.</p>"
                + "<p>You may notice three important things <ul>"
                + "<li>The images on the ruler are disabled for a while.</li>"
                + "<li>The button on the ruler are disabled for a while.</li>"
                + "<li>The interface only works after the images and the button are enabled.</li>"
                + "</ul></p>"
                + "<p>At the next screen you will try the interface by yourself. Please, pay attention on the "
                + "images. It is recommended that you <em>observe all the images</em>. "
                + "So, we especially recommend that you scroll towards the upper and lower bounds.</p>"
                + "<p>Lets make a first attempt to familiarize you with the interface.</p>"
                + "<p>Click the OK button to continue.</p>"
                + "</body>"
                + "</html>";
        return txt;
    }

    @Override
    protected Class getNextActivity() {
        return PreTrainingQualityRulerActivity.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return false;
    }
}
