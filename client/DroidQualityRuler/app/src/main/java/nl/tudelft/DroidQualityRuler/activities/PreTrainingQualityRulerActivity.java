package nl.tudelft.DroidQualityRuler.activities;

import nl.tudelft.DroidQualityRuler.controllers.UserActionController;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/18/14.at TU Delft
 */
public class PreTrainingQualityRulerActivity extends AbstractQualityRulerActivity {
    @Override
    void initUserActionController() {
        this.userActionController = new UserActionController();
        this.userActionController.setMaxVideoCounter(3);
    }

    @Override
    String getTypeOfActivity() {
        return "PreTraining";
    }

    @Override
    protected Class getNextActivity() {
        return TrainingInstructionActivity5.class;
    }
}
