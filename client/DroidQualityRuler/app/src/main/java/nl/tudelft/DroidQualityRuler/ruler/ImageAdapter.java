package nl.tudelft.DroidQualityRuler.ruler;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import nl.tudelft.DroidQualityRuler.R;
import nl.tudelft.DroidQualityRuler.controllers.ImagesController;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 6/19/14.
 */
public class ImageAdapter extends BaseAdapter {
    private Integer[] thumbnails;
    private Context context;

    public ImageAdapter(Context c) {
        this.thumbnails = ImagesController.getImages();
        context = c;
    }

    public int getCount() {
        return this.thumbnails.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(context);
        final int wrapContent = Gallery.LayoutParams.WRAP_CONTENT;
        i.setImageResource(this.thumbnails[position]);
        i.setAdjustViewBounds(true);
        i.setLayoutParams(new Gallery.LayoutParams(wrapContent, wrapContent));
        i.setBackgroundResource(R.drawable.ic_launcher);
        return i;
    }

}
