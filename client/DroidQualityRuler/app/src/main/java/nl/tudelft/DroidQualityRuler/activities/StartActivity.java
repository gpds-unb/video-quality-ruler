package nl.tudelft.DroidQualityRuler.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import nl.tudelft.DroidQualityRuler.R;
import nl.tudelft.DroidQualityRuler.controllers.SocketConnectionController;
import nl.tudelft.DroidQualityRuler.controllers.SoundsController;
import nl.tudelft.DroidQualityRuler.data.OpenFileRequestPacket;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/7/14.at TU Delft
 */
public class StartActivity extends Activity {
    Button btnNextScreen;
    EditText inputName;
    EditText inputAge;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startscreen);
        createInputNameListener();
        createButtonListener();
        createInputAgeListener();
    }

    private void requestOpenFile(final String name, final String age) {
        new Thread() {
            public void run() {
                OpenFileRequestPacket fileRequest = new OpenFileRequestPacket();
                fileRequest.setStepCode("OpenFile");
                fileRequest.setName(name);
                fileRequest.setAge(Integer.parseInt(age));
                SocketConnectionController.send(fileRequest.toJSONString());
            }
        }.start();
    }

    private String getInputName() {
        return inputName.getText().toString();
    }

    private String getInputAge() {
        return inputAge.getText().toString();
    }

    private void createButtonListener() {
        btnNextScreen = (Button) findViewById(R.id.btnNextScreen);
        btnNextScreen.setEnabled(false);
        btnNextScreen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent nextScreen = new Intent(getApplicationContext(), FirstPresentationInstructionActivity.class);
                String name = getInputName();
                String age = getInputAge();
                nextScreen.putExtra("name", name);
                nextScreen.putExtra("age", age);
                requestOpenFile(name, age);
                startActivity(nextScreen);
            }
        });
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            check();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            check();
        }

        @Override
        public void afterTextChanged(Editable s) {
            check();
        }

        private void check() {
            if (getInputAge().length() >= 2 && getInputName().length() >= 2)
                btnNextScreen.setEnabled(true);
            else
                btnNextScreen.setEnabled(false);
        }
    };

    private void createInputNameListener() {
        inputName = (EditText) findViewById(R.id.name);
        inputName.addTextChangedListener(textWatcher);
    }

    private void createInputAgeListener() {
        inputAge = (EditText) findViewById(R.id.age);
        inputAge.addTextChangedListener(textWatcher);
    }
}
