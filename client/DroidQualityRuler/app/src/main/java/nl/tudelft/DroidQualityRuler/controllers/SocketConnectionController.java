package nl.tudelft.DroidQualityRuler.controllers;

import android.util.Log;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import nl.tudelft.DroidQualityRuler.network.ClientSocket;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/8/14.at TU Delft
 */
public class SocketConnectionController {
    private static SocketConnectionController instance = null;
    //private static ClientSocket sock = new ClientSocket("10.0.2.2", 8080);
    private static ClientSocket sock = new ClientSocket("192.168.1.134", 8080);

    public static SocketConnectionController getInstance() {
        if (instance == null) {
            instance = new SocketConnectionController();
        }
        return instance;
    }

    public static void send(String v) {
        Log.d("SocketConnectionController.v", v);
        sock.send(v);
    }
}
