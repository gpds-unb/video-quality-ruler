package nl.tudelft.DroidQualityRuler.activities;

import nl.tudelft.DroidQualityRuler.controllers.UserActionController;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class Main2QualityRulerActivity extends AbstractQualityRulerActivity {
    @Override
    void initUserActionController() {
        this.userActionController = new UserActionController();
        this.userActionController.setMaxVideoCounter(25);
    }

    @Override
    String getTypeOfActivity() {
        return "Main2";
    }

    @Override
    protected Class getNextActivity() {
        return FinalActivity.class;
    }
}
