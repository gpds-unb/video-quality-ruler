package nl.tudelft.DroidQualityRuler.activities;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/8/14.at TU Delft
 */
public class TrainingInstructionActivity extends AbstractInstructionsActivity {
    @Override
    protected String getText() {
        String txt = "<html>"
                + "<head>"
                + "<title></title>"
                + "</head>"
                + "<body>"
                + "<h1>Guided Practicing</h1>"
                + "<p>Have you noticed the artifacts/defects in the videos?</p>"
                + "<p>Are the artifacts different from each other?</p>"
                + "<p>If they are different, how different are they?</p>"
                + "<p>We want to know the answers for these questions and "
                + "You are here to help us in this mission.</p>"
                + "<p>In the next screen We will show You how to proceed. </p>"
                + "<p>Please, click on OK button to continue and <em>read carefully</em> the next instructions.</p>"
                + "</body>"
                + "</html>";
        return String.format(txt, name);
    }


    @Override
    protected Class getNextActivity() {
        return TrainingInstructionActivity2.class;
    }

    @Override
    protected Integer getWaitingTimeForEnableButton() {
        return 1000;
    }

    @Override
    protected Boolean isBepedInstruction() {
        return false;
    }
}
