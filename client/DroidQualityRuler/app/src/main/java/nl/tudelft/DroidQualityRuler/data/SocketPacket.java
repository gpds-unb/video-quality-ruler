package nl.tudelft.DroidQualityRuler.data;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 7/10/14.at TU Delft
 */
public class SocketPacket {
    private String stepCode = "None";
    protected JSONObject js = new JSONObject();

    public String getStepCode() {
        return stepCode;
    }

    public void setStepCode(String stepCode) {
        this.stepCode = stepCode;
    }

    public String toJSONString() {
        String ret;
        try {
            createJSONString();
            ret = js.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            ret = "{}";
        }
        return ret;
    }

    protected void createJSONString() throws JSONException {
        js.put("stepCode", getStepCode().toString());
    }
}
