package nl.tudelft.DroidQualityRuler.controllers;

import nl.tudelft.DroidQualityRuler.R;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 6/19/14.at TU Delft
 */
public class ImagesController {
    private static ImagesController instance = null;
    private ArrayList<Integer> images = new ArrayList<Integer>();
    private ArrayList<String> imagesNames = new ArrayList<String>();

    protected ImagesController() {

    }

    public static ImagesController getInstance() {
        if (instance == null) {
            instance = new ImagesController();
        }
        return instance;
    }

    public static void loadImages() {
        releaseImages();
        Field[] drawables = R.drawable.class.getFields();
        for (Field img : drawables) {
            try {
                queueImageIfInTurn(img);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }

    private static void releaseImages() {
        getInstance().images.clear();
    }

    private static void queueImageIfInTurn(Field f) throws IllegalAccessException {
        String imageName = f.getName();
        String validPrefix = "boat_blur";
        if (imageName.startsWith(validPrefix)) {
            queueImageByID(f.getInt(f));
        }
    }

    private static void queueImageByID(Integer id) {
        getInstance().images.add(id);
    }

    public static Integer[] getImages() {
        ArrayList<Integer> imagesFromInstance = getInstance().images;
        Integer[] imgs = imagesFromInstance.toArray(new Integer[imagesFromInstance.size()]);
        return imgs;
    }

    public static Integer getImageAt(Integer position) {
        Integer[] imgs = getImages();
        return imgs[position];
    }
}