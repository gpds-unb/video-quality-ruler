package nl.tudelft.DroidQualityRuler.data;

import org.json.JSONException;

/**
 * Created by Pedro Garcia <sawp@sawp.com.br>
 * on 6/26/14.at TU Delft
 */
public class ClientDataPacket extends SocketPacket {
    private Integer video = 0;
    private Integer image = 0;

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }


    protected void createJSONString() throws JSONException {
        super.createJSONString();
        js.put("video", getVideo().toString());
        js.put("image", getImage().toString());
    }
}
