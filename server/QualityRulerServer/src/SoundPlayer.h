#ifndef SOUNDPLAYER_H
#define SOUNDPLAYER_H

#include <string>

class SoundPlayer
{
    public:
        SoundPlayer();
        virtual ~SoundPlayer();
        static void playSound(std::string);
    protected:
    private:
};

#endif // SOUNDPLAYER_H
