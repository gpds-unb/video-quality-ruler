#ifndef VIDEOCONTROLLER_H
#define VIDEOCONTROLLER_H

#include <string>
#include <vector>

class VideoController
{
    public:
        VideoController();
        virtual ~VideoController();
        void loadVideos(void);
        void loadVideos(std::string);
        std::string getVideoPathAt(int);
        static bool isPlaying(void);
        static void setPlaying(bool);
    protected:
    private:
        unsigned int videoCounter;
        static bool playing;
        std::vector<std::string> videos;
};

#endif // VIDEOCONTROLLER_H
