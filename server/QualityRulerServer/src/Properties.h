#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>


using namespace std;
using namespace boost::property_tree;

namespace Properties
{
    void init(void);

    namespace ControlPanel
    {
        extern string BUTTON_LOCATION;
        extern string WINDOW_NAME;
        extern string TRACKBAR_RULER_NAME;
        extern int SLIDER_MAX;
        void init(ptree);
    } // namespace ControlPanel

    namespace VideoController
    {
        extern string VIDEOS_TRAINING_DIRECTORY;
        extern string VIDEOS_FREEVIEWING_DIRECTORY;
        extern string VIDEOS_DIRECTORY;
        extern string VIDEO_COUNTER_DIRECTORY;
        extern string VIDEO_INSTRUCTIONS_DIRECTORY;
        extern string VIDEO_HOWTO_DIRECTORY;
        void init(ptree);
    } // namespace VideoControler

    namespace SoundPlayer
    {
        extern string SOUNDS_DIRECTORY;
        void init(ptree);
    } //namespace SoundPlayer

    namespace ResultsPath
    {
        extern string RESULTS_DIRECTORY;
        void init(ptree);
    } //namespace ResultsPath
};

#endif // PROPERTIES_H
