#include "FileHelper.h"

using namespace std;

namespace FileHelper
{
    namespace fs = boost::filesystem;

    vector<string> getDirectoryNamesInDirectory(string path)
    {
        fs::path dir(path);
        vector<fs::path> dirs;
        copy_if(fs::directory_iterator(dir), fs::directory_iterator(), back_inserter(dirs), [](fs::path p) {
            return fs::is_directory(p);
        });
        vector<string> paths(dirs.size());
        transform(dirs.begin(), dirs.end(), paths.begin(), [](fs::path it) -> string {
            return it.generic_string();
        });
        return paths;
    }

    vector<string> getFileNamesInDirectory(string path)
    {
        fs::path dir(path);
        vector<fs::path> v;
        copy(fs::directory_iterator(dir), fs::directory_iterator(), back_inserter(v));
        vector<string> paths(v.size());
        transform(v.begin(), v.end(), paths.begin(), [](fs::path it) -> string
        {
            return it.generic_string();
        });
        return paths;
    }

    vector<string> filterFileTypes(vector<string> files, string extension)
    {
        vector<string> filtered;
        copy_if(files.begin(), files.end(), back_inserter(filtered), [&extension](string f) {
            return f.substr(f.find_last_of(".") + 1) == extension;
        });
        sort(filtered.begin(), filtered.end());
        return filtered;
    }

    vector<string> filterFileTypes(vector<string> files)
    {
        vector<string> filtered1 = filterFileTypes(files, "avi");
        vector<string> filtered2 = filterFileTypes(files, "mp4");
        copy(filtered1.begin(), filtered1.end(), back_inserter(filtered2));
        return filtered2;
    }
}


