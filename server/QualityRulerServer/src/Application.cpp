#include "crow.h"
#include "Application.h"
#include "Properties.h"
#include "SoundPlayer.h"
#include <ctime>
#include <unistd.h>
#include <tuple>
#include <typeinfo>
#include "Poco/URI.h"


using namespace std;

bool Application::playedCountdownAtMain1 = false;
bool Application::playedCountdownAtMain2 = false;
bool Application::playedCountdownAtTraining = false;
bool Application::playedPreTraining = false;
bool Application::playedMainSession1 = false;
bool Application::playedMainSession2 = false;
bool Application::playedTrainingSession = false;
bool Application::playedFreeViewingSession = false;
bool Application::playedHowTo = false;
int Application::videoPlaying = 0;
int Application::requestedVideo = 0;
string Application::videoPlayingName = "";
string Application::stepCode = "None";
string Application::stepRunning = "None";
ofstream Application::outputFile;

Application::Application() {
    //ctor
}

Application::~Application() {
    //dtor
}


void Application::init(void) {
    auto parseStringToJSON = [](string data) -> Json::Value {
        Json::Value json;
        Json::Reader reader;
        reader.parse(data, json, false);
        return json;
    };

    auto getTime = [](void) -> string {
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(buffer, 80, " %d-%m-%Y %I:%M:%S", timeinfo);
        string str(buffer);
        return str;
    };

    auto saveToFile = [&getTime](int videoCode, int imageCode) -> void {
        std::stringstream out;

        out << getTime() << "\t" << stepCode << "\t" << "\t" << videoPlayingName;
        out << "\t" << videoCode << "\t" << imageCode << "\n";

        std::cout << out.str() << std::endl;

        outputFile << getTime() << "\t" << stepCode << "\t" << "\t" << videoPlayingName;
        outputFile << "\t" << videoCode << "\t" << imageCode << "\n";
        outputFile.flush();
    };

    auto openFile = [](string filename) {
        outputFile.open(Properties::ResultsPath::RESULTS_DIRECTORY + filename);
    };

    auto timestr = []() -> string {
        std::stringstream strm;
        strm << time(0);
        return strm.str();
    };

    auto doOpenFile = [&openFile, &timestr](Json::Value json) {
        if (json.isMember("name") && json.isMember("age")) {
            string name = json["name"].asString();
            string age = json["age"].asString();
            openFile(name + "_" + age + "_" + timestr() + ".txt");
        }
    };

    auto doChangeRequestVideo = [](Json::Value json) -> pair<int, int> {
        pair<int, int> p(0, 0);
        if (json.isMember("video") && json.isMember("image")) {
            int videoCode = stoi(json["video"].asString());
            int imageCode = stoi(json["image"].asString());
            p.first = videoCode;
            p.second = imageCode;
            requestedVideo = videoCode + 1;
        }
        return p;
    };

    auto doSaveFile = [&saveToFile, &doChangeRequestVideo](Json::Value json) {
        pair<int, int> p = doChangeRequestVideo(json);
        int videoCode = p.first;
        int imageCode = p.second;
        saveToFile(videoCode, imageCode);
    };

    auto doQualityRulerSession = [&doSaveFile, &doChangeRequestVideo](Json::Value json) {
        doSaveFile(json);
        doChangeRequestVideo(json);
    };

    auto processJSON = [&doOpenFile, &doQualityRulerSession](Json::Value json) {
        std::cout << "ProcessJSON: " << json << std::endl;
        stepCode = json["stepCode"].asString();
        auto isMain = [](string s) -> bool { return s.find("Main") != std::string::npos; };
        if (isMain(stepCode) || stepCode == "Training")
            doQualityRulerSession(json);
        else if (stepCode == "OpenFile")
            doOpenFile(json);
    };

    auto getInitializedScreen = [](void) -> Screen {
        Screen screen;
        screen.setScreenName("WINDOW");
        screen.setScreenPosition(0, 0);
        return screen;
    };

    static Screen screen = getInitializedScreen();

    auto closeWindow = [](void) {
        screen.closeWindow();
    };

    auto playVideo = [](int pos, string location) -> void {
        auto getInitializedVideoController = [&location](void) -> VideoController {
            VideoController vc;
            vc.loadVideos(location);
            return vc;
        };


        auto getVideoLocation = [&getInitializedVideoController](int position) -> string {
            VideoController vc = getInitializedVideoController();
            return vc.getVideoPathAt(position);
        };

        auto isSameVideo = []() -> bool {
            return videoPlaying == requestedVideo;
        };

        auto isSameStage = []() -> bool {
            return stepCode == stepRunning;
        };

        auto playFrames = [&isSameVideo, &isSameStage](Screen &screen) -> void {
            while (!screen.isFinished() && isSameVideo() && isSameStage()) {
                screen.playSingleFrame();
                cv::waitKey(1000.0 / 60.0);
                //cv::waitKey(1);
            }
        };

        string videoLocation = getVideoLocation(pos);
        videoPlayingName = videoLocation;
        screen.setVideoLocation(videoLocation);
        screen.load();
        playFrames(screen);
    };

    auto startVideo = [&playVideo, &closeWindow]() -> void {
        auto playMain = [&playVideo](string part) {
            stepCode = stepRunning = "Main" + part;
            if (part == "1")
                playedMainSession1 = true;
            if (part == "2")
                playedMainSession2 = true;
            if (videoPlaying != requestedVideo)
                videoPlaying = requestedVideo;
            playVideo(requestedVideo, Properties::VideoController::VIDEOS_DIRECTORY + "/part" + part);
        };

        auto playInstruction = [&playVideo]() {
            videoPlaying = requestedVideo = 0;
            playVideo(videoPlaying, Properties::VideoController::VIDEO_INSTRUCTIONS_DIRECTORY);
        };

        auto playCounter = [&playVideo]() {
            videoPlaying = requestedVideo = 0;
            SoundPlayer::playSound("counter.wav");
            playVideo(videoPlaying, Properties::VideoController::VIDEO_COUNTER_DIRECTORY);
        };

        auto playFreeViewing = [&playCounter, &playInstruction, &playVideo]() {
            auto playSequences = [&playVideo]() {
                const int MAXPlayingVideos = 7;
                for (int v = 0; v < MAXPlayingVideos; v++) {
                    videoPlaying = requestedVideo = v;
                    playVideo(v, Properties::VideoController::VIDEOS_FREEVIEWING_DIRECTORY);
                };
            };
            stepRunning = "FreeViewing";
            playCounter();
            playSequences();
            playInstruction();
            playedFreeViewingSession = true;
        };

        auto playTraining = [&playVideo]() {
            stepRunning = "Training";
            playedTrainingSession = true;
            if (videoPlaying != requestedVideo)
                videoPlaying = requestedVideo;
            playVideo(requestedVideo, Properties::VideoController::VIDEOS_TRAINING_DIRECTORY);
        };

        auto closeSession = []() {
            stepCode = "None";
            stepRunning = "None";
            playedFreeViewingSession = false;
            playedTrainingSession = false;
            playedMainSession1 = false;
            playedMainSession2 = false;
            playedHowTo = false;
            playedPreTraining = false;
            playedCountdownAtTraining = false;
            playedCountdownAtMain1 = false;
            playedCountdownAtMain2 = false;
        };

        auto startRequesTraining = [&playCounter, &playVideo]() {
            stepRunning = "StartRequestTraining";
            if (!playedCountdownAtTraining) {
                playCounter();
                playedCountdownAtTraining = true;
            } else {
                videoPlaying = requestedVideo = 0;
                playVideo(requestedVideo, Properties::VideoController::VIDEOS_TRAINING_DIRECTORY);
            }
        };

        auto startRequesMain = [&playCounter, &playVideo](string part) {
            stepRunning = "StartRequestMain" + part;
            if (part == "1" && !playedCountdownAtMain1) {
                playCounter();
                playedCountdownAtMain1 = true;
            } else if (part == "2" && !playedCountdownAtMain2) {
                playCounter();
                playedCountdownAtMain2 = true;
            } else {
                videoPlaying = requestedVideo = 0;
                playVideo(requestedVideo, Properties::VideoController::VIDEOS_DIRECTORY + "/part" + part);
            }
        };


        auto playHowTo = [&playCounter, &playInstruction, &playVideo]() {
            auto playSequences = [&playVideo]() {
                videoPlaying = requestedVideo = 0;
                playVideo(videoPlaying, Properties::VideoController::VIDEO_HOWTO_DIRECTORY);
            };
            stepRunning = "HowTo";
            playCounter();
            playSequences();
            playInstruction();
            playedHowTo = true;
        };

        auto playPreTraining = [&playCounter, &playInstruction]() {
            stepRunning = "PreTraining";
            playInstruction();
        };

        while (true) {
            if (stepCode == "FreeViewing" && !playedFreeViewingSession)
                playFreeViewing();
            else if (stepCode == "Main1")
                playMain("1");
            else if (stepCode == "Main2")
                playMain("2");
            else if (stepCode == "PreTraining" && !playedPreTraining)
                playPreTraining();
            else if (stepCode == "Training")
                playTraining();
            else if (stepCode == "HowTo" && !playedHowTo)
                playHowTo();
            else if (stepCode == "CloseFile")
                outputFile.close();
            else if (stepCode == "CloseWindow")
                closeWindow();
            else if (stepCode == "CloseSession")
                closeSession();
            else if (stepCode == "StartRequestTraining" && !playedTrainingSession)
                startRequesTraining();
            else if (stepCode == "StartRequestMain1" && !playedMainSession1)
                startRequesMain("1");
            else if (stepCode == "StartRequestMain2" && !playedMainSession2)
                startRequesMain("2");
        }
    };

    auto socketListener = [&processJSON, &parseStringToJSON](void) -> void {
        crow::SimpleApp app;
        CROW_ROUTE(app, "/<string>")
                ([&processJSON, &parseStringToJSON](string encoded) {
                    string decoded;
                    Poco::URI::decode(encoded, decoded);
                    Json::Value json = parseStringToJSON(decoded);
                    processJSON(json);
                    std::cout << stepCode << std::endl;
                    return crow::response(stepCode);
                });
        app.port(8080).multithreaded().run();
    };

    boost::thread sockThread(socketListener);
    startVideo();
    sockThread.join();
}

void Application::init2(void) {
}

