#ifndef SCREEN_H
#define SCREEN_H

#include <iostream>
#include <vector>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;

class Screen {
public:
     Screen();
     virtual ~Screen();
     void setScreenName(string);
     void setVideoLocation(string);
     void setScreenPosition(int, int);
     void load(void);
     void playSingleFrame(void);
     bool isFinished(void);
     double getVideoFPS(void);
     void closeWindow(void);
protected:
     std::string screenName;
     std::string videoLocation;
     int xPosition;
     int yPosition;
     int tPosition;
     double totalFrames;
     double fps;
     cv::VideoCapture cap;
     vector<cv::Mat> frames;
     bool isVideoChanged;
private:
     void testCapture(cv::VideoCapture&);
     void loadFrames(cv::VideoCapture&);
};

#endif // SCREEN_H
