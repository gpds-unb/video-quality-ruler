#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include "Socket.h"

class ServerSocket : private Socket
{
 public:

  ServerSocket ( int port );
  ServerSocket (){};
  virtual ~ServerSocket();

  const ServerSocket& operator << ( std::string& );
  const ServerSocket& operator >> ( std::string& );

  void accept ( ServerSocket& );
  bool recv(std::string&);
};


#endif // SERVERSOCKET_H
