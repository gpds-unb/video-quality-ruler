#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <vector>

using namespace std;

namespace FileHelper
{
    vector<string> getDirectoryNamesInDirectory(string path);
    vector<string> getFileNamesInDirectory(string path);
    vector<string> filterFileTypes(vector<string> files, string extension);
    vector<string> filterFileTypes(vector<string> files);
};

#endif // FILEHELPER_H
