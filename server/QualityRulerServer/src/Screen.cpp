#include "Screen.h"

#include "opencv2/highgui/highgui.hpp"

double Screen::getVideoFPS(void)
{
     return this->fps;
}

bool Screen::isFinished(void)
{
     return (this->tPosition >= this->totalFrames);
}

void Screen::testCapture(cv::VideoCapture& cap)
{
     if(!cap.isOpened()) {
          cout << "Cannot open the video file" << endl;
          exit(0);
     }
}

void Screen::loadFrames(cv::VideoCapture& cap)
{
     //this->frames.clear();
     //cv::Scalar gray(153, 153, 153);
     for(int f=0; f < this->totalFrames; f++) {
          cv::Mat frame;
          try {
              if(cap.read(frame)) {
                  cv::Mat bf;
                  cv::copyMakeBorder(frame, bf, 180, 180, 320, 320, cv::BORDER_CONSTANT, cv::Scalar(153, 153, 153));
                  this->frames.push_back(bf);
              }
           } catch(cv::Exception ex) {
                string msg = "Cannot read the frame from video file ";
                string problem = this->videoLocation;
                string at = this->screenName;
                cout << msg << problem << " at " << at << endl;
                cout << "" << ex.what() << endl;
           }
    }
//    cv::Mat frame = this->frames[0].clone();
//    frame = gray;
//    for(int f=0; f < 3000; f++) {
//      this->frames.push_back(frame);
//    }
}


//void Screen::playSingleFrame(void)
//{
//     cv::Mat f = this->frames[this->tPosition];
//     cv::imshow(this->screenName, f);
//     this->tPosition++;
//}


void Screen::playSingleFrame()
{
     cv::VideoCapture& cap = this->cap;
     cv::Mat frame;
     cv:: Scalar gray = cv::Scalar(153, 153, 153);
     try {
          if(cap.read(frame)) {
              cv::Mat bf;
              cv::copyMakeBorder(frame, bf, 180, 180, 320, 320, cv::BORDER_CONSTANT, gray);
              cv::imshow(this->screenName, bf);
              this->tPosition++;
          }
     } catch(cv::Exception ex) {
          string msg = "Cannot read the frame from video file ";
          string problem = this->videoLocation;
          string at = this->screenName;
          cout << msg << problem << " at " << at << endl;
          cout << "" << ex.what() << endl;
     }
}

void Screen::closeWindow(void)
{
    this->tPosition = this->totalFrames + 1;
    cv::namedWindow(this->screenName, CV_WINDOW_NORMAL);
    cv::setWindowProperty(this->screenName, CV_WND_PROP_AUTOSIZE, CV_WINDOW_AUTOSIZE);
    cv::setWindowProperty(this->screenName, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    //cv::destroyWindow(this->screenName);
    cv::destroyAllWindows();
}

void Screen::load(void)
{
     //cv::namedWindow(this->screenName, CV_WINDOW_AUTOSIZE);
     cv::namedWindow(this->screenName, CV_WINDOW_NORMAL);
     cv::moveWindow(this->screenName, this->xPosition, this->yPosition);
     cv::setWindowProperty(this->screenName, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
     cv::VideoCapture temp(this->videoLocation);
     int numberOfFrames = (int) temp.get(CV_CAP_PROP_FRAME_COUNT);
     double fps = temp.get(CV_CAP_PROP_FRAME_COUNT);
     temp.set(CV_CAP_PROP_BUFFERSIZE, (double) numberOfFrames);
     this->cap = temp;
     this->totalFrames = numberOfFrames;
     this->fps = fps;
     this->tPosition = 0;
     testCapture(cap);
//     loadFrames(cap);
}

void Screen::setScreenName(string name)
{
     this->screenName = name;
}

void Screen::setVideoLocation(string location)
{
//     if (location.compare(this->videoLocation) != 0)
//        isVideoChanged = false;
//     else
//        isVideoChanged = true;
     this->videoLocation = location;
}

void Screen::setScreenPosition(int x, int y)
{
     this->xPosition = x;
     this->yPosition = y;
}

Screen::Screen()
{

}

Screen::~Screen()
{
     //dtor
}
