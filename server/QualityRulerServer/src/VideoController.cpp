#include "VideoController.h"
#include "FileHelper.h"
#include "Properties.h"
#include <algorithm>
#include <iostream>

using namespace std;

bool VideoController::playing = false;

VideoController::VideoController()
{
//    loadVideos();
}

VideoController::~VideoController()
{
    //dtor
}

bool VideoController::isPlaying(void)
{
    return VideoController::playing;
}

void VideoController::setPlaying(bool p)
{
    VideoController::playing = p;
}

void VideoController::loadVideos(void)
{
    std::vector<string> files = FileHelper::getFileNamesInDirectory(Properties::VideoController::VIDEOS_DIRECTORY);
    this->videos = FileHelper::filterFileTypes(files, "avi");
}

void VideoController::loadVideos(string location)
{
    std::vector<string> files = FileHelper::getFileNamesInDirectory(location);
    std::vector<string> avis = FileHelper::filterFileTypes(files, "avi");
    std::vector<string> mp4s = FileHelper::filterFileTypes(files, "mp4");
    std::vector<string> videos = avis;
    srand(0);
    random_shuffle(videos.begin(), videos.end());
    videos.insert(videos.end(), mp4s.begin(), mp4s.end());
    this->videos = videos;
}


std::string VideoController::getVideoPathAt(int position)
{
    return this->videos[position];
}
