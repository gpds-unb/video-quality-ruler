/*
 * InterfaceLister.hpp
 *     Lists network interfaces.
 *
 * Author
 *     Andrew Brown
 */
#ifndef INTERFACELISTER_HPP
#define INTERFACELISTER_HPP
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <Poco/Net/NetworkInterface.h>
#define LOOPBACK_ADDRESS "127.0.0.1"



/// @defgroup common
/**
 * @ingroup common
 * @brief
 *     Lists network interfaces.
 */
class InterfaceLister {


        public :

                static std::string getDefault();
                static std::vector<std::string> list();
                static std::vector<std::string> sortedList();
};


#endif
