#include "Properties.h"
#include<iostream>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

using namespace std;
using namespace boost::property_tree;

namespace Properties
{

    void init(void)
    {
        ptree pTree;
        ini_parser::read_ini("config.ini", pTree);
        ControlPanel::init(pTree);
        VideoController::init(pTree);
        SoundPlayer::init(pTree);
        ResultsPath::init(pTree);
    }

    namespace ControlPanel
    {
        string BUTTON_LOCATION;
        string WINDOW_NAME;
        string TRACKBAR_RULER_NAME;
        int SLIDER_MAX;

        void init(ptree pt)
        {
            Properties::ControlPanel::WINDOW_NAME = pt.get<string>("ControlPanel.WINDOW_NAME");
        }
    } // namespace ControlPanel

    namespace VideoController
    {
        string VIDEOS_DIRECTORY;
        string VIDEOS_FREEVIEWING_DIRECTORY;
        string VIDEOS_TRAINING_DIRECTORY;
        string VIDEO_COUNTER_DIRECTORY;
        string VIDEO_INSTRUCTIONS_DIRECTORY;
        string VIDEO_HOWTO_DIRECTORY;

        void init(ptree pt)
        {
            Properties::VideoController::VIDEOS_DIRECTORY
                = pt.get<string>("VideoController.VIDEOS_DIRECTORY");
            Properties::VideoController::VIDEOS_FREEVIEWING_DIRECTORY
                = pt.get<string>("VideoController.VIDEOS_FREE_VIEWING_DIRECTORY");
            Properties::VideoController::VIDEOS_TRAINING_DIRECTORY
                = pt.get<string>("VideoController.VIDEOS_TRAINING_DIRECTORY");
            Properties::VideoController::VIDEO_COUNTER_DIRECTORY
                = pt.get<string>("VideoController.VIDEO_COUNTER_DIRECTORY");
            Properties::VideoController::VIDEO_INSTRUCTIONS_DIRECTORY
                = pt.get<string>("VideoController.VIDEO_INSTRUCTIONS_DIRECTORY");
            Properties::VideoController::VIDEO_HOWTO_DIRECTORY
                = pt.get<string>("VideoController.VIDEO_HOWTO_DIRECTORY");
        }
    } // namespace VideoControler

    namespace SoundPlayer
    {
        string SOUNDS_DIRECTORY;

        void init(ptree pt)
        {
            Properties::SoundPlayer::SOUNDS_DIRECTORY
                = pt.get<string>("SoundPlayer.SOUNDS_DIRECTORY");
        }
    } //namespace SoundPlayer

    namespace ResultsPath
    {
        string RESULTS_DIRECTORY;
        void init(ptree pt)
        {
            Properties::ResultsPath::RESULTS_DIRECTORY
                = pt.get<string>("ResultsPath.RESULTS_DIRECTORY");
        }
    } //namespace ResultsPath
}
