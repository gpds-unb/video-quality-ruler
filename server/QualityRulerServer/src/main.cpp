#include "Application.h"
#include "Properties.h"

int main(int argc, char* argv[])
{
    Properties::init();
    Application::init();

    return 0;
}


