#ifndef APPLICATION_H
#define APPLICATION_H

#include <string>
#include <iostream>
#include <ctime>
#include <json/json.h>
#include "ServerSocket.h"
#include "SocketException.h"
#include "VideoController.h"
#include "Properties.h"
#include "Screen.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>

class Application
{
    public:
        static void init(void);
        static void init2(void);
    protected:
    private:
        Application();
        ~Application();

        static bool playedCountdownAtMain1;
        static bool playedCountdownAtMain2;
        static bool playedCountdownAtTraining;
        static bool playedPreTraining;
        static bool playedHowTo;
        static bool playedFreeViewingSession;
        static bool playedTrainingSession;
        static bool playedMainSession1;
        static bool playedMainSession2;
        static int videoPlaying;
        static int requestedVideo;
        static std::string stepCode;
        static std::string stepRunning;
        static std::string videoPlayingName;
        static std::ofstream outputFile;
};

#endif // APPLICATION_H
