#include "SoundPlayer.h"
#include "Properties.h"
#include <SDL/SDL_mixer.h>
#include <string>
#include <iostream>
#include <boost/thread.hpp>
#include <stdlib.h>
#include <SDL/SDL.h>

void SoundPlayer::playSound(std::string f)
{
    auto s = [] (std::string file) {
        Mix_Chunk *sound = NULL;        //Pointer to our sound, in memory
        int channel;                //Channel on which our sound is played

        int audio_rate = 22050;            //Frequency of audio playback
        Uint16 audio_format = AUDIO_S16SYS;     //Format of the audio we're playing
        int audio_channels = 2;            //2 channels = stereo
        int audio_buffers = 4096;        //Size of the audio buffers in memory

        SDL_Init(SDL_INIT_AUDIO);

        //Initialize SDL_mixer with our chosen audio settings
        if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
            printf("Unable to initialize audio: %s\n", Mix_GetError());
        }

        //Load our WAV file from disk
        sound = Mix_LoadWAV(file.c_str());
        if(sound == NULL) {
            printf("Unable to load WAV file: %s\n", Mix_GetError());
        }


        //Play our sound file, and capture the channel on which it is played
        channel = Mix_PlayChannel(-1, sound, 0);
        if(channel == -1) {
            printf("Unable to play WAV file: %s\n", Mix_GetError());
        }

        //Wait until the sound has stopped playing
        while(Mix_Playing(channel) != 0);

        //Release the memory allocated to our sound
        Mix_FreeChunk(sound);

        //Need to make sure that SDL_mixer and SDL have a chance to clean up
        Mix_CloseAudio();
        SDL_Quit();
    };
    boost::thread soundThread(s, Properties::SoundPlayer::SOUNDS_DIRECTORY + f);
}

SoundPlayer::SoundPlayer()
{
    //ctor
}

SoundPlayer::~SoundPlayer()
{
    //dtor
}
