/*
 * InterfaceLister.cpp
 *     Lists network interfaces.
 *
 * Author
 *     Andrew Brown
 */
#include "InterfaceLister.h"



/**
 * Returns the default address from the list of network interfaces.
 *
 * If the loopback address is in the list, but there are other addresses
 * available, one of the others is returned instead.
 */
std::string InterfaceLister::getDefault() {

        std::vector<std::string> addresses;

        // Get addresses, return first
        addresses = sortedList();
        if (!addresses.empty())
                return addresses[0];
        else
                return "";
}



/**
 * Returns the addresses of the network interfaces on this computer.
 *
 * If the loopback address (127.0.0.1) is found, it is put at the end of the
 * list.
 */
std::vector<std::string> InterfaceLister::list() {

        bool loopbackFound = false;
        std::vector<Poco::Net::NetworkInterface> interfaces;
        std::vector<std::string> addresses;
        std::string iAddress;

        // List all addresses
        interfaces = Poco::Net::NetworkInterface::list();
        for (int i=0; i<interfaces.size(); i++) {
                iAddress = interfaces[i].address().toString();
                if (iAddress.compare(LOOPBACK_ADDRESS) != 0)
                        addresses.push_back(interfaces[i].address().toString());
                else
                        loopbackFound = true;
        }
        if (loopbackFound)
                addresses.push_back(LOOPBACK_ADDRESS);
        return addresses;
}



/**
 * Returns the addresses of the network interfaces on this computer.
 *
 * Addresses are sorted by their class.  Class B addresses are first, followed
 * by Class C, and then Class A, including the loopback address, which should
 * always be last.  Note that Class D and E addresses are not stored.
 */
std::vector<std::string> InterfaceLister::sortedList() {

        char *token, *address;
        int length, firstOctet;
        std::vector<std::string> all, classA, classB, classC;

        // For each address
        all = list();
        for (int i=0; i<all.size(); i++) {

                // Get first octet
                length = all[i].length();
                address = new char[length+1];
                strcpy(address, all[i].c_str());
                token = strtok(address, ".");
                firstOctet = atoi(token);

                // Sort by first octet
                if (firstOctet <= 127)
                        classA.push_back(all[i]);
                else if (firstOctet <= 191)
                        classB.push_back(all[i]);
                else if (firstOctet <= 223)
                        classC.push_back(all[i]);
                delete[] address;
        }

        // Combine
        all.clear();
        for (int i=0; i<classB.size(); i++)
                all.push_back(classB[i]);
        for (int i=0; i<classC.size(); i++)
                all.push_back(classC[i]);
        for (int i=0; i<classA.size(); i++)
                all.push_back(classA[i]);
        return all;
}
