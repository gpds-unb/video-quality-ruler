# VQR - Video Quality Ruler

The Video Quality Ruler enables efficient calibrated measurement over a wide range of video quality, using a graphical user interface with a slider for selecting a ruler image with quality equal to that of a test video. 

## Contents

1.  [Description](#description)
2.  [License](#license)
3.  [Requirements](#requirements)
4.  [Build and Installation](#installation)
5.  [Instructions](#instructions)
6.  [Contact](#contact)
7.  [Contributing](#contributing)

## Description

In this project we implement a subjective video quality assessment method called video quality ruler (VQR) that can be used to determine the perceived quality of video sequences. The described method is an extension of the ISO 20462, which is a standardized method to assess image quality. The VQR method provides a interface with a set of images. The subjects assess the video using these images and compare the subjective perceived video quality with the perceived quality of these images. The images are calibrated to form a numerical scale in units of just noticeable differences (JNDs), which make us able to analyze and compare both subjective video and image stimuli. 

The system used to provide the VQR interface is designed as a client-server architecture. The communication is implemented via sockets with an unidirectional data flow from client (tablet) to server (PC).

The server side is responsible for receive the requests from client, convert this requests in commands and perform them. Basically, the commands consists in save the experiment states, collect the experimental data inserted using the tablet, and change the videos. The server was implemented in C++11 for performance. Therefore, some libraries are required for proper compilation, as stated below.

The client side is responsible for catch the user commands, format it in a structured data, and send it to the server. It was implemented in Java 6 with compatibility with Android SDK. Therefore, if the system has the Android SDK properly installed, additional libraries are not requires. 




# License

This project is released under [GNU GPL version
2.](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)


# Requirements

The only requirement to build the **<u>client</u>** is an environment configured with Android SDK. These are the recommendations:

1. **Operating Systems**
    * Windows XP (32-bit), Vista (32- or 64-bit), or Windows 7 (32- or 64-bit)
    * Mac OS X 10.8.5 or later
    * Linux
        - 64-bit distribution capable of running 32-bit applications
        - GNU C Library (glibc) 2.11 or later is required.
        - Tested on Ubuntu 12.04 (Precise Pangolin) 32-bit and on Ubuntu 14.04 (Trusty Tahr) 64-bit.
        
2. ** Development tools **
    * Oracle JDK 6 (JRE alone is not sufficient)
    * Apache Ant 1.8 or later
    * A version of the Android platform
    * Android Platform-tools
    * Android SDK Tools
    * A version of the Android system image for the emulator

3. ** Recommended IDEs **
    * Eclipse + ADT plugin
    * Android Studio
    * IntelliJ IDEA
        - This project was developed using IntelliJ IDEA 13 (Community Edition). The project also was build correctly using IntelliJ IDEA 14 (Community Edition).

The **<u>server</u>** requires a compiler compatible with C++11 and several libraries. These  libraries and compilers were used in development, testing and production environments (Ubuntu 14.04 LTS 64-bit):

1. ** Compiler **
    * g++ 4.8.2, or
    * clang 3.4-1
2. ** Boost C++ Libraries ** 
    * Boost System (1.54)
    * Boost Filesystem (1.54)
    * Boost Thread (1.55)
3. ** Simple DirectMedia Layer Libraries ** 
    * libsdl2 (2.0-0)
    * libsdl2-mixer (2.0-0)
4. ** OpenCV Libraries **
    * The VQR was developed and tested using OpenCV 2.4.11 compiled from source. 
    * Follow these instructions to build this library following the instructions below.
5. ** Additional Libraries**
    * libjsoncpp (0.6.0)
6. ** ffmpeg**
    * The experiments were performed using ffmpeg version 2.6.
    * Use ffmpeg compiled from source (see next section).

## Install OpenCV from source

### 1. Make Directory

> `$ cd ~`  
> `$ mkdir opencv`  
> `$ opencv/`  

### 2. Remove any pre-installed ffmpeg and x264

> `$ sudo apt-get -qq remove ffmpeg x264 libx264-dev`  

### 3. Install Dependencies

> `$ sudo apt-get update`  
> `$ sudo apt-get install build-essential checkinstall git cmake libfaac-dev libjack-jackd2-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libsdl1.2-dev libtheora-dev libva-dev libvdpau-dev libvorbis-dev libx11-dev libxfixes-dev libxvidcore-dev texi2html yasm zlib1g-dev`  

### 4. Download and install gstreamer

> `$ sudo apt-get install libgstreamer0.10-0 libgstreamer0.10-dev gstreamer0.10-tools gstreamer0.10-plugins-base libgstreamer-plugins-base0.10-dev gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-plugins-bad gstreamer0.10-ffmpeg`  

### 5. Download and install gtk

> `$ sudo apt-get install libgtk2.0-0 libgtk2.0-dev`  

### 6. Download and install libjpeg

> `$ sudo apt-get install libjpeg8 libjpeg8-dev`  

### 7. Download and install install x264

Download the current stable version of x264 at [ftp://ftp.videolan.org/pub/videolan/x264/snapshots/](ftp://ftp.videolan.org/pub/videolan/x264/snapshots/). For example, if the latest  version is `x264-snapshot-20141218-2245-stable.tar.bz2`, you can install it using the following commands

> `$ wget ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-20141218-2245-stable.tar.bz2`  
> `$ tar vzxf x264-snapshot-20141218-2245-stable.tar.bz2`  
> `$ cd x264-snapshot-20120528-2245-stable`  

Configure the compilation to enable the shared lib (required for 64-bit versions of Linux)

> `$ ./configure --enable-shared --enable-pic`  

OBS:  You know you need these options if you get the following error when compiling OpenCV:

        [ 25%] Building CXX object modules/highgui/CMakeFiles/opencv_highgui.dir/src/bitstrm.cpp.o
Linking CXX shared library ../../lib/libopencv_highgui.so
/usr/bin/ld: /usr/local/lib/libavcodec.a(avpacket.o): relocation R_X86_64_32S against `av_destruct_packet' can not be used when making a shared object; recompile with -fPIC
/usr/local/lib/libavcodec.a: could not read symbols: Bad value

### 8. Download and install ffmpeg

Download ffmpeg version from [http://ffmpeg.org/download.html](http://ffmpeg.org/download.html). For example, if the last version is `ffmpeg-2.6.2.tar.bz2`, run the following commands:

> `$ wget http://ffmpeg.org/releases/ffmpeg-2.6.2.tar.bz2`  
> `$ tar vzxf ffmpeg-2.6.2.tar.bz2`  
> `$ cd ffmpeg-2.6.2`  

Configure ffmpeg:

> `$ ./configure --enable-gpl --enable-libfaac --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libtheora --enable-libvorbis --enable-libx264 --enable-libxvid --enable-nonfree --enable-postproc --enable-version3 --enable-x11grab --enable-shared --enable-pic`  
> `$ make`  
> `$ sudo make install`

### 9. Download and install Video for Linux (v4l)

Download the last version of v4l from [http://www.linuxtv.org/downloads/v4l-utils/](http://www.linuxtv.org/downloads/v4l-utils/). For example, if you downloaded the version `v4l-utils-0.8.8`, run the following commands:

> `$ wget http://www.linuxtv.org/downloads/v4l-utils/v4l-utils-0.8.8.tar.bz2`  
> `$ tar xvf v4l-utils-0.8.8.tar.bz2`  
> `$ cd v4l-utils-0.8.8`  
> `$ make`  
> `$ sudo make install`  

### 10. Download and install OpenCV

Get the last version of OpenCV from [http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/](http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/). This project requires version 2.4.11 or above. Therefore, if you are using the version 2.4.11, run the following commands on terminal:

> `$ wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.11/opencv-2.4.11.zip`  
> `$ unzip OpenCV-2.4.11.zip`  
> `$ cd OpenCV-2.4.11/`  
> `$ mkdir build`  
> `$ cd build`  

Run cmake to create compilation with support to the extensions that are available on your system (TBB, OpenMP, CUDA, OpenGL, etc)

> `$ cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_VTK=ON -D WITH_FFMPEG=OFF`  
> `$ make`  
> `$ sudo make install`  

Configure the shared libraries:

> `$ sudo echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf`  
> `$ sudo ldconfig /etc/ld.so.conf`


# Build and Installation

Clone the last version
> `$ hg clone https://bitbucket.org/kuraiev/videoqualityruler`  

Go to the project root
> `$ cd videoqualityruler`  

## Build client

Compile the client code
> `$ cd client/DroidQualityRuler`  
> `$ ant debug`  

Load the client on device
> `$ adb install bin/DroidQualityRuler-debug.apk`  

## Build server

Compile the server code 
> `$ cd server/QualityRulerServer/`  
> `$ make`  

To run the server, just go to __server/QualityRulerServer/bin__ directory and run
> `$ ./QualityRulerServer`

# Instructions

## Running Client

All customizable information in client side is stored at __client/DroidQualityRuler/assets/Properties.properties__. There are two variables in this file. The first one, *server_ip*, is responsible to point to the server's address. If both client and server are in the same network range and if server has a name, this variable can contain the server's name too. The second variable, *server_port*, contains the listening port at server side.

## Running Server

The main server's configuration file is __server/QualityRulerServer/config.ini__. This file contains the variables which points to file system directories with the data. For example, if the sounds that will be played during the session are stored at *~/mysounds*, the variable **SOUNDS_DIRECTORY** must be set with this value.


# Contact

Please send all comments, questions, reports and suggestions (especially if you would like to contribute) to Pedro Garcia **sawp@sawp.com.br**.

# Contributing

If you would like to contribute with new algorithms, increment the code performance, documentation or another kind of modifications, please contact me. The only requirements are: keep the code licensed by [GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).